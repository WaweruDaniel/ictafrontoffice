<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />
<%
	session.removeAttribute("xmlcnf");
	session.invalidate();
%>
    <!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js ie6 oldie  blank-false " lang="en-US"> <![endif]-->
<!--[if IE 7]>
<html class="no-js ie7 oldie  blank-false " lang="en-US"> <![endif]-->
<!--[if IE 8]>
<html class="no-js ie8 oldie  blank-false " lang="en-US"> <![endif]-->
<!--[if IE 9]>
<html class="no-js ie9 oldie  blank-false " lang="en-US"> <![endif]-->
<!--[if gt IE 9]><!-->
<html class="  blank-false  js flexbox canvas canvastext webgl no-touch geolocation postmessage no-websqldatabase indexeddb hashchange history draganddrop websockets rgba hsla multiplebgs backgroundsize borderimage borderradius boxshadow textshadow opacity cssanimations csscolumns cssgradients no-cssreflections csstransforms csstransforms3d csstransitions fontface generatedcontent video audio localstorage sessionstorage webworkers applicationcache svg inlinesvg smil svgclippaths js_active  vc_desktop  vc_transform  vc_transform  vc_transform "
      idmmzcc-ext-docid="1237288960" style="" lang="en-US"><!--<![endif]-->
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">

    <!-- META TAGS -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!-- LINK TAGS -->
    <link rel="pingback" href="http://sacco.dewcis.co.ke/xmlrpc.php">
    <title>ICT Authority</title>
    <link rel="dns-prefetch" href="http://maps.google.com/">
    <link rel="dns-prefetch" href="http://ajax.googleapis.com/">
    <link rel="dns-prefetch" href="http://s.w.org/">
    <script type="text/javascript" style="color: rgb(0, 0, 0);">
        window._wpemojiSettings = {
            "baseUrl": "https:\/\/s.w.org\/images\/core\/emoji\/2\/72x72\/",
            "ext": ".png",
            "svgUrl": "https:\/\/s.w.org\/images\/core\/emoji\/2\/svg\/",
            "svgExt": ".svg",
            "source": {"concatemoji": "http:\/\/sacco.dewcis.co.ke\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.6.1"}
        };
        !function (a, b, c) {
            function d(a) {
                var c, d, e, f, g, h = b.createElement("canvas"), i = h.getContext && h.getContext("2d"), j = String.fromCharCode;
                if (!i || !i.fillText)return !1;
                switch (i.textBaseline = "top", i.font = "600 32px Arial", a) {
                    case"flag":
                        return i.fillText(j(55356, 56806, 55356, 56826), 0, 0), !(h.toDataURL().length < 3e3) && (i.clearRect(0, 0, h.width, h.height), i.fillText(j(55356, 57331, 65039, 8205, 55356, 57096), 0, 0), c = h.toDataURL(), i.clearRect(0, 0, h.width, h.height), i.fillText(j(55356, 57331, 55356, 57096), 0, 0), d = h.toDataURL(), c !== d);
                    case"diversity":
                        return i.fillText(j(55356, 57221), 0, 0), e = i.getImageData(16, 16, 1, 1).data, f = e[0] + "," + e[1] + "," + e[2] + "," + e[3], i.fillText(j(55356, 57221, 55356, 57343), 0, 0), e = i.getImageData(16, 16, 1, 1).data, g = e[0] + "," + e[1] + "," + e[2] + "," + e[3], f !== g;
                    case"simple":
                        return i.fillText(j(55357, 56835), 0, 0), 0 !== i.getImageData(16, 16, 1, 1).data[0];
                    case"unicode8":
                        return i.fillText(j(55356, 57135), 0, 0), 0 !== i.getImageData(16, 16, 1, 1).data[0];
                    case"unicode9":
                        return i.fillText(j(55358, 56631), 0, 0), 0 !== i.getImageData(16, 16, 1, 1).data[0]
                }
                return !1
            }

            function e(a) {
                var c = b.createElement("script");
                c.src = a, c.type = "text/javascript", b.getElementsByTagName("head")[0].appendChild(c)
            }

            var f, g, h, i;
            for (i = Array("simple", "flag", "unicode8", "diversity", "unicode9"), c.supports = {
                everything: !0,
                everythingExceptFlag: !0
            }, h = 0; h < i.length; h++)c.supports[i[h]] = d(i[h]), c.supports.everything = c.supports.everything && c.supports[i[h]], "flag" !== i[h] && (c.supports.everythingExceptFlag = c.supports.everythingExceptFlag && c.supports[i[h]]);
            c.supports.everythingExceptFlag = c.supports.everythingExceptFlag && !c.supports.flag, c.DOMReady = !1, c.readyCallback = function () {
                c.DOMReady = !0
            }, c.supports.everything || (g = function () {
                c.readyCallback()
            }, b.addEventListener ? (b.addEventListener("DOMContentLoaded", g, !1), a.addEventListener("load", g, !1)) : (a.attachEvent("onload", g), b.attachEvent("onreadystatechange", function () {
                "complete" === b.readyState && c.readyCallback()
            })), f = c.source || {}, f.concatemoji ? e(f.concatemoji) : f.wpemoji && f.twemoji && (e(f.twemoji), e(f.wpemoji)))
        }(window, document, window._wpemojiSettings);
    </script>
    <script src="assets/wp-emoji-release.js" type="text/javascript"></script>
    <style type="text/css">
        img.wp-smiley,
        img.emoji {
            display: inline !important;
            border: none !important;
            box-shadow: none !important;
            height: 1em !important;
            width: 1em !important;
            margin: 0 .07em !important;
            vertical-align: -0.1em !important;
            background: none !important;
            padding: 0 !important;
        }
    </style>
    <link rel="stylesheet" id="contact-form-7-css" href="assets/styles.css"
          type="text/css" media="all">
    <link rel="stylesheet" id="jquery-ui-theme-css" href="assets/jquery-ui.css"
          type="text/css" media="all">
    <link rel="stylesheet" id="jquery-ui-timepicker-css"
          href="assets/jquery-ui-timepicker-addon.css" type="text/css" media="all">
    <link rel="stylesheet" id="gglcptch-css" href="assets/gglcptch.css"
          type="text/css" media="all">
    <link rel="stylesheet" id="rs-plugin-settings-css" href="assets/settings.css"
          type="text/css" media="all">
    <style id="rs-plugin-settings-inline-css" type="text/css">
        #rs-demo-id {
        }
    </style>
    <link rel="stylesheet" id="chosen-css" href="assets/chosen.css" type="text/css"
          media="all">
    <link rel="stylesheet" id="wp-job-manager-frontend-css"
          href="assets/frontend_002.css" type="text/css" media="all">
    <link rel="stylesheet" id="math-captcha-frontend-css" href="assets/frontend.css"
          type="text/css" media="all">
    <link rel="stylesheet" id="focuson-style-css" href="assets/style.css"
          type="text/css" media="all">
    <link rel="stylesheet"  href="assets/fonts/font-awesome/css/font-awesome.min.css"
          type="text/css" media="all">
    <link rel="stylesheet" id="js_composer_front-css" href="assets/js_composer.css"
          type="text/css" media="all">
    <!--Google font Welcom login-->
    <link href="https://fonts.googleapis.com/css?family=Pacifico" rel="stylesheet">
    <link href="assets/css/style-theme.css" rel="stylesheet">
    <script type="text/javascript" src="assets/js/jquery_003.js"></script>
    <script type="text/javascript" src="assets/js/jquery-migrate.js"></script>
    <script type="text/javascript" src="assets/js/jquery_006.js"></script>
    <script type="text/javascript" src="assets/js/jquery_004.js"></script>
    <script type="text/javascript">
        /* <![CDATA[ */
        var wc_add_to_cart_params = {
            "ajax_url": "\/wp-admin\/admin-ajax.php",
            "wc_ajax_url": "\/login\/?wc-ajax=%%endpoint%%",
            "i18n_view_cart": "View Cart",
            "cart_url": "http:\/\/sacco.dewcis.co.ke",
            "is_cart": "",
            "cart_redirect_after_add": "no"
        };
        /* ]]> */
    </script>
    <script type="text/javascript" src="assets/js/add-to-cart.js"></script>
    <script type="text/javascript" src="assets/js/woocommerce-add-to-cart.js"></script>
    <script type="text/javascript" src="assets/js/js"></script>
    <script type="text/javascript" src="assets/js/modernizr.js"></script>
  

    <link rel="wlwmanifest" type="application/wlwmanifest+xml"
       
    <meta name="generator" content="WordPress 4.6.1">
    <meta name="generator" content="WooCommerce 2.6.8">
  
   
    <style type="text/css">.recentcomments a {
        display: inline !important;
        padding: 0 !important;
        margin: 0 !important;
    }</style>
    <meta name="generator" content="Powered by Visual Composer - drag and drop page builder for WordPress.">
    <!--[if lte IE 9]>
    <link rel="stylesheet" type="text/css"
          href="http://sacco.dewcis.co.ke/wp-content/plugins/js_composer/assets/css/vc_lte_ie9.min.css" media="screen">
    <![endif]--><!--[if IE  8]>
    <link rel="stylesheet" type="text/css"
          href="http://sacco.dewcis.co.ke/wp-content/plugins/js_composer/assets/css/vc-ie8.min.css" media="screen">
    <![endif]-->
    <meta name="generator"
          content="Powered by Slider Revolution 5.2.6 - responsive, Mobile-Friendly Slider Plugin for WordPress with comfortable drag and drop interface.">
   
    <noscript>
        <style type="text/css"> .wpb_animate_when_almost_visible {
            opacity: 1;
        }</style>
    </noscript>



    <script type="text/javascript" charset="UTF-8" src="assets/js/common.js"></script>
    <script type="text/javascript" charset="UTF-8" src="assets/js/util.js"></script>
    <script type="text/javascript" charset="UTF-8" src="assets/js/stats.js"></script>
</head>
<body class="page page-id-143 page-template page-template-page-vc page-template-page-vc-php focuson-null24-net wpb-js-composer js-comp-ver-4.12 vc_responsive">
<!-- general wrap start -->
<div id="gen-wrap">
    <!-- wrap start -->
    <div id="wrap" class="nz-wide">

        <header class="header mob-header cart-false nz-clearfix">
            <div class="mob-header-top nz-clearfix">
                <div class="container">
                    <div class="logo logo-mob">
                       <a href="http://icta.go.ke/" title="Front Office">
                                <img style="max-width:149px;max-height:38px;"
                                     src="assets/sacco-logo-transparent-mobile.png"
                                     alt="Front Office">
                            </a>
                    </div>
                    <span class="mob-menu-toggle"></span>
                </div>
            </div>
        </header>

        <div class="mob-header-content nz-clearfix">

            <span class="mob-menu-toggle2"></span>

            <div class="custom-scroll-bar ps-container">

               <nav class="mob-menu nz-clearfix">
                    <ul id="mob-header-menu" class="menu">
                        <li id="menu-item-58"
                            class="menu-item menu-item-type-post_type menu-item-object-page menu-item-58"><a
                                href="http://icta.go.ke/"target="_blank"><span class="mi"></span><span
                                class="txt">Home</span><span class="di icon-arrow-down9"></span></a></li>
                            </ul>
                        </li>
                        <li id="menu-item-59"
                            class="menu-item menu-item-type-post_type menu-item-object-page menu-item-59"><a
                                href="http://icta.go.ke/"target="_blank"><span class="mi"></span><span class="txt">Contact Us</span><span
                                class="di icon-arrow-down9"></span></a></li>
                    </ul>
                </nav>



                <div class="social-links nz-clearfix">
                    <a class="icon-facebook" href="https://www.facebook.com/ICTAuthorityKE" title="facebook"
                       target="_blank"></a><a class="icon-twitter" href="https://twitter.com/ICTAuthorityKE" title="twitter"
                                              target="_blank"></a><a class="icon-linkedin"
                                                                     href="https://www.linkedin.com/company/kenya-ict-board"
                                                                     title="linkedin" target="_blank"></a></div>

                <div class="search nz-clearfix">
                    <form action="http://icta.go.ke/"target="_blank" method="get">
                        <fieldset>
                            <input name="s" id="s" data-placeholder="Search for..." value="Search for..." type="text">
                            <input id="searchsubmit" value="Search" type="submit">
                        </fieldset>
                    </form>
                </div>

                <div class="ps-scrollbar-x-rail" style="width: 320px; display: none; left: 0px;">
                    <div class="ps-scrollbar-x" style="left: 0px; width: 0px;"></div>
                </div>
                <div class="ps-scrollbar-y-rail" style="top: 0px; height: 665px; display: none; right: 3px;">
                    <div class="ps-scrollbar-y" style="top: 0px; height: 0px;"></div>
                </div>
            </div>
        </div>
        <div class="mob-overlay">&nbsp;</div>

        <header class="header desk version1 stuck-false top-true sl-true search-true cart-false iversion-dark effect-underline subeffect-ghost fixed-true fiversion-dark">

            <div class="header-content">


                <div class="header-top">
                    <div class="container nz-clearfix">


                       <div class="social-links header-top-social-links nz-clearfix">
                            <a class="icon-facebook" href="https://www.facebook.com/ICTAuthorityKE" title="facebook"
                               target="_blank"></a><a class="icon-twitter" href="https://twitter.com/ICTAuthorityKE"
                                                      title="twitter" target="_blank"></a><a class="icon-linkedin"
                                                                                             href="https://www.linkedin.com/company/kenya-ict-board"
                                                                                             title="linkedin"
                                                                                             target="_blank"></a></div>

                    </div>
                </div>


                <div class="header-body">
                    <div class="container nz-clearfix">

                        <div class="logo logo-desk">
                           <a href="http://icta.go.ke/"target="_blank" title="Front Office">
                                <img style="max-width:149px;max-height:38px;"
                                     src="assets/sacco-logo-transparent-mobile.png"
                                     alt="Front Office">
                            </a>
                        </div>

                        <div class="logo logo-desk-fixed">
                            <a href="http://icta.go.ke/"target="_blank" title="Front Office">
                                <img style="max-width:149px;max-height:38px;"
                                     src="assets/sacco-logo-transparent-mobile.png"
                                     alt="Front Office">
                            </a>
                        </div>

                        <%--<div class="search-toggle"></div>--%>
                        <div class="search">
                            <form action="http://icta.go.ke/"target="_blank" method="get">
                                <fieldset>
                                    <input name="s" id="s" data-placeholder="Search for..." value="Search for..."
                                           type="text">
                                    <input id="searchsubmit" value="Search" type="submit">
                                </fieldset>
                            </form>
                        </div>

                    <nav class="header-menu desk-menu nz-clearfix">
                            <ul id="header-menu" class="menu">
                                <li id="menu-item-58" class="menu-item menu-item-type-post_type menu-item-object-page"
                                    data-mm="false" data-mmc="2"><a href="http://icta.go.ke/"target="_blank"><span
                                        class="mi"></span><span class="txt">Home</span></a></li>
                                        <li id="menu-item-59"
                            class="menu-item menu-item-type-post_type menu-item-object-page menu-item-59"><a
                                href="http://icta.go.ke/"target="_blank"><span class="mi"></span><span class="txt">Contact Us</span><span
                                class="di icon-arrow-down9"></span></a></li>
                            
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </header>
        <div class="page-content-wrap">
            <!--<header class="rich-header page-header version1 icon-dark" data-parallax="false"-->
            <!--style="background-color:#f4f4f4;color:#777777;">-->
            <!--<div class="container nz-clearfix">-->

            <!--<h1 style="">Login</h1>-->

            <!--<div style="color:#999999;" class="nz-breadcrumbs nz-clearfix"><a href="http://sacco.dewcis.co.ke/"-->
            <!--rel="v:url" property="v:title">Home</a><span-->
            <!--class="current">Login</span></div>-->

            <!--</div>-->
            <!--</header>-->

            <!-- content start -->
            <div id="nz-content" class="content nz-clearfix padding-false">
                <div class="container page-full-width">
                    <!-- post start -->
                    <div id="post-143" class="post-143 page type-page status-publish hentry">
                        <section class="page-content nz-clearfix">
                            <div class="nz-section horizontal autoheight-false animate-false full-width-false custom-padding-2"
                                 data-img-width="1600" data-img-height="751" data-animation-speed="35000"
                                 data-parallax="true" style="padding-top:100px;padding-bottom:75px;">
                                <div style="background-size: cover; background-image: url(&quot;./assets/images/background.png&quot;); background-repeat: no-repeat; background-position: center top; background-attachment: scroll; transform: translateY(-111px);"
                                     class="parallax-container"></div>
                                <div class="container">
                                    <div class="nz-row">
                                        <div class="col vc_col-sm-6 col6  col-animate-false" style="" data-effect="fade"
                                             data-align="left">
                                            <div class="col-inner" style=""></div>
                                        </div>
                                        <div class="col vc_col-sm-6 col6  col-animate-false" style="" data-effect="fade"
                                             data-align="left">
                                            <div class="col-inner" style="">
                                                <p role="form" class="wpcf7" id="wpcf7-f151-p143-o1" dir="ltr"
                                                   lang="en-US">
                                                <div class="screen-reader-response"></div>
                                                <form action="/login/#wpcf7-f151-p143-o1" method="post"
                                                      class="wpcf7-form" novalidate="novalidate">
                                                    <div style="display: none;">
                                                        <input name="_wpcf7" value="151" type="hidden">
                                                        <input name="_wpcf7_version" value="4.5.1" type="hidden">
                                                        <input name="_wpcf7_locale" value="en_US" type="hidden">
                                                        <input name="_wpcf7_unit_tag" value="wpcf7-f151-p143-o1"
                                                               type="hidden">
                                                        <input name="_wpnonce" value="ddb46d3418" type="hidden">
                                                    </div>
                                                    <p><h1 class="title" style="color:#00A651;"><b>Front Office System</b></h1></p><br>
                                                    <!--Error alert-->
                                                    <p class="error" style="max-width: 60%;">
                                                        <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
                                                        <strong>Password/Username</strong> don't match!
                                                    <p>
                                                    <p>
                                                        <!--<i class="fa fa-lock"></i>
                                                        <a href="application.jsp?view=2:0"> Forgot password? </a>-->
                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        <i class="fa fa-edit"></i>
                                                        <a href="index.jsp" style="color: #000000;"> <b>Try Again</b> </a>
                                                    </p>

                                                    <div class="wpcf7-response-output wpcf7-display-none"></div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>
                    <div class="nz-section horizontal autoheight-false animate-false full-width-false "
                         data-animation-speed="35000" data-parallax="false"
                         style="background-color:#f9f9f9;padding-top:10px;padding-bottom:20px;">
                        <div class="container">
                            <div class="nz-row">
                                <div class="col vc_col-sm-12 col12  col-animate-false" style=""
                                     data-effect="fade" data-align="left">
                                    <div class="col-inner" style="">
                                        <div style="" class="nz-column-text nz-clearfix "><p></p>

                                            <h3 style="text-align: center; font-size: 28px; line-height: 38px;">
                                                FEATURES OF <span style="color: #FE2E2E; font-weight: bold;">FRONT OFFICE SYSTEM</span>
                                            </h3>

                                            <div class="gap nz-clearfix" style="height:35px">&nbsp;</div>
                                        </div>
                                        <div class="nz-content-box nz-clearfix animate-none" data-columns="3"
                                             data-animate="false">
                                            <div id="nz-box-1" class="  nz-box animate-item">
                                                <div class="nz-box-wrap">
                                                    <style>#nz-box-1 .box-icon {
                                                        color: #c2c2c2;
                                                    }

                                                    #nz-box-1:hover .box-icon {
                                                        color: #90c847 !important;
                                                    }</style>
                                                    <div class="box-icon-wrap">
                                                        <div class="box-icon icon-cog"></div>
                                                    </div>
                                                    <div class="box-data"><p></p>

                                                        <h3><span style="font-size: 16px; line-height: 26px;">Visitors SingUp</span>
                                                        </h3>

                                                        <p><span style="color: #777777;">Front office Web Application enables efficient management of visitors at the reception within ICT Authority premises,by capturing their records and providing well organised Report <br>
</span><a class="box-more" href="#">Read more<span class="icon-arrow-right2"></span></a></p></div>
                                                </div>
                                            </div>
                                            <div id="nz-box-2" class="  nz-box animate-item">
                                                <div class="nz-box-wrap">
                                                    <style>#nz-box-2 .box-icon {
                                                        color: #c2c2c2;
                                                    }

                                                    #nz-box-2:hover .box-icon {
                                                        color: #90c847 !important;
                                                    }</style>
                                                    <div class="box-icon-wrap">
                                                        <div class="box-icon icon-stats"></div>
                                                    </div>
                                                    <div class="box-data"><p></p>

                                                        <h3><span style="font-size: 16px; line-height: 26px;">Letters </span>
                                                        </h3>

                                                        <p><span style="color: #777777;">The Application will enable efficient management of Letters within the Organization, by keeping record and helping in easily tracing them on files.<br>
</span></p>

                                                        <p><a class="box-more" href="#">Read more<span
                                                                class="icon-arrow-right2"></span></a></p></div>
                                                </div>
                                            </div>
                                            <div id="nz-box-3" class="  nz-box animate-item">
                                                <div class="nz-box-wrap">
                                                    <style>#nz-box-3 .box-icon {
                                                        color: #c2c2c2;
                                                    }

                                                    #nz-box-3:hover .box-icon {
                                                        color: #90c847 !important;
                                                    }</style>
                                                    <div class="box-icon-wrap">
                                                        <div class="box-icon icon-support"></div>
                                                    </div>
                                                    <div class="box-data"><p></p>

                                                        <h3><span style="font-size: 16px; line-height: 26px;">Boardroom Booking</span>
                                                        </h3>

                                                        <p><span style="color: #777777;">The system also allows staff and management to book board rooms as well as view the status of the board rooms i.e.

whether they are free or booked.<br>
</span><a class="box-more" href="#">Read more<span class="icon-arrow-right2"></span></a></p></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                   
                <!-- post end -->
        
        <!-- content end -->
        <!-- footer start -->
        <footer class="footer">
            <div class="footer-widget-area nz-clearfix">
                <div class="container">
                </div>
            </div>
            <div class="footer-info-area nz-clearfix">
                <div class="container">
                    <div class="footer-copyright nz-clearfix">
                        2016 &copy; ICT Authority |<a href="http://www.icta.go.ke"target="_blank">All Rights Reserved |</a> Powered by ICTA
                    </div>
                    <div class="social-links">
                        <a class="icon-facebook" href="https://www.facebook.com/ICTAuthorityKE" title="facebook"
                           target="_blank"></a><a class="icon-twitter" href=https://twitter.com/ICTAuthorityKE"
                                                  title="twitter" target="_blank"></a><a class="icon-linkedin"
                                                                                         href="https://www.linkedin.com/company/kenya-ict-board""
                                                                                         title="linkedin"
                                                                                         target="_blank"></a></div>
                    <nav class="footer-menu nz-clearfix">
                        <ul id="footer-menu" class="menu">
                            <li id="menu-item-64"
                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-64"><a
                                    href="http://icta.go.ke/"target="_blank">Home</a></li>
                            <li id="menu-item-65"
                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-65"><a
                                    href="http://icta.go.ke/"target="_blank">Contact Us</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </footer>
        <!-- footer end -->
    </div>
</div>
<!-- wrap end -->
</div>
<a id="top" href="#wrap" class=""></a>
<!-- general wrap end -->


<link rel="stylesheet" id="vc_google_fonts_abril_fatfaceregular-css"
      href="assets/css.css" type="text/css" media="all">
<script type="text/javascript" src="assets/jquery_002.js"></script>
<script type="text/javascript">
    /* <![CDATA[ */
    var _wpcf7 = {
        "loaderUrl": "http:\/\/sacco.dewcis.co.ke\/wp-content\/plugins\/contact-form-7\/images\/ajax-loader.gif",
        "recaptcha": {"messages": {"empty": "Please verify that you are not a robot."}},
        "sending": "Sending ..."
    };
    /* ]]> */
</script>
<script type="text/javascript" src="assets/scripts.js"></script>
<script type="text/javascript" src="assets/core.js"></script>
<script type="text/javascript" src="assets/datepicker.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function (jQuery) {
        jQuery.datepicker.setDefaults({
            "closeText": "Close",
            "currentText": "Today",
            "monthNames": ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
            "monthNamesShort": ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
            "nextText": "Next",
            "prevText": "Previous",
            "dayNames": ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
            "dayNamesShort": ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
            "dayNamesMin": ["S", "M", "T", "W", "T", "F", "S"],
            "dateFormat": "MM d, yy",
            "firstDay": 1,
            "isRTL": false
        });
    });
</script>
<script type="text/javascript" src="assets/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript" src="assets/js/widget.js"></script>
<script type="text/javascript" src="assets/js/mouse.js"></script>
<script type="text/javascript" src="assets/js/slider.js"></script>
<script type="text/javascript" src="assets/js/button.js"></script>
<script type="text/javascript" src="assets/js/jquery-ui-sliderAccess.js"></script>
<script type="text/javascript" src="assets/js/jquery.js"></script>
<script type="text/javascript">
    /* <![CDATA[ */
    var woocommerce_params = {
        "ajax_url": "\/wp-admin\/admin-ajax.php",
        "wc_ajax_url": "\/login\/?wc-ajax=%%endpoint%%"
    };
    /* ]]> */
</script>
<script type="text/javascript" src="assets/woocommerce.js"></script>
<script type="text/javascript" src="assets/jquery_005.js"></script>
<script type="text/javascript">
    /* <![CDATA[ */
    var wc_cart_fragments_params = {
        "ajax_url": "\/wp-admin\/admin-ajax.php",
        "wc_ajax_url": "\/login\/?wc-ajax=%%endpoint%%",
        "fragment_name": "wc_fragments"
    };
    /* ]]> */
</script>
<script type="text/javascript" src="assets/cart-fragments.js"></script>
<script type="text/javascript" src="assets/comment-reply.js"></script>
<script type="text/javascript" src="assets/jquery_007.js"></script>
<script type="text/javascript" src="assets/controller.js"></script>
<script type="text/javascript" src="assets/wp-embed.js"></script>
<script type="text/javascript" src="assets/js_composer_front.js"></script>

</body>
</html>