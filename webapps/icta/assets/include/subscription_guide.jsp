<div class="row">
    <div class="col-md-12">
        <!-- BEGIN Portlet PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-green-sharp">
                    <i class="icon-info font-green-sharp"></i>
                    <!--<span class="caption-subject bold uppercase"> Portlet</span>-->
                    <span class="caption-helper"><b>Guidelines</b></span>
                </div>
                <div class="actions">
                    <a href="javascript:;" class="btn btn-circle btn-default btn-icon-only fullscreen"></a>
                </div>
            </div>
            <div class="portlet-body">
                <div class="scroller" style="height:460px" data-rail-visible="1" data-rail-color="yellow" data-handle-color="#a1b2bd">
                    <h4></h4>
                    <ol>
                        <li>Fill in <b>all the fields</b>, correctly. Ensure you provide valid Information.</li>
                        <li>Click on <b>Submit Button</b>,After you are done with filling the form.</li>
                         <li>Kindly Submit your <b>National ID/Passport</b> to Reception.</li>
                        <li>Click <a href="index.jsp?xml=icta.xml"><b> HERE </b></a>to LOGIN <b>ICTA Users Only</b>.</li>
                    </ol>
                </div>
            </div>
        </div>
        <!-- END Portlet PORTLET-->
    </div>
</div>