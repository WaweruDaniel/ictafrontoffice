---Function 

CREATE OR REPLACE FUNCTION department_manage (character varying, character varying, character varying, character varying)
RETURNS character varying AS $$
DECLARE
	v_approval		integer;
	v_department_id 	integer;
	v_response		varchar(255);

BEGIN
	v_department_id := cast($1 as int);
	v_approval := cast($3 as int);
	v_response := '';
	
	IF(v_approval = 1) THEN
		DELETE FROM department WHERE department_id = cast($1 as int);
		v_response = 'Record removed successfully';

	END IF ;
	RETURN v_response ;
END;
$$ LANGUAGE plpgsql ;


CREATE OR REPLACE FUNCTION visitor_manage(character varying, character varying, character varying, character varying)
RETURNS character varying AS $$
DECLARE
	v_approval		integer;
	v_visitor_id 		integer;
	v_response		varchar(255);

BEGIN
	v_visitor_id  := cast($1 as int);
	v_approval := cast($3 as int);
	v_response := '';
	
	IF(v_approval = 1) THEN
		DELETE FROM visitor WHERE visitor_id  = cast($1 as int);
		v_response = 'Record removed successfully';

	END IF ;
	RETURN v_response ;
END;
$$ LANGUAGE plpgsql ;


CREATE OR REPLACE FUNCTION status_manage(character varying, character varying, character varying, character varying)
RETURNS character varying AS $$
DECLARE
	v_approval		integer;
	v_status_id 		integer;
	v_response		varchar(255);

BEGIN
	v_status_id  := cast($1 as int);
	v_approval := cast($3 as int);
	v_response := '';
	
	IF(v_approval = 1) THEN
		DELETE FROM status WHERE status_id  = cast($1 as int);
		v_response = 'Record removed successfully';

	END IF ;
	RETURN v_response ;
END;
$$ LANGUAGE plpgsql ;

CREATE OR REPLACE FUNCTION purpose_manage(character varying, character varying, character varying, character varying)
RETURNS character varying AS $$
DECLARE
	v_approval		integer;
	v_purpose_id 		integer;
	v_response		varchar(255);

BEGIN
	v_purpose_id  := cast($1 as int);
	v_approval := cast($3 as int);
	v_response := '';
	
	IF(v_approval = 1) THEN
		DELETE FROM purpose WHERE purpose_id  = cast($1 as int);
		v_response = 'Record removed successfully';

	END IF ;
	RETURN v_response ;
END;
$$ LANGUAGE plpgsql ;


