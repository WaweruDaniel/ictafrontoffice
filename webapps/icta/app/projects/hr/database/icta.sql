---Project Database File

ALTER TABLE entitys
ADD department_id  integer references department;


ALTER TABLE entitys
ADD employee_position   varchar(100);


-----Tables 

CREATE TABLE boardroom(
	boardroom_id   	         serial primary key,
	boardroom_name    		  varchar(100)
);

CREATE TABLE department(
	department_id   	         serial primary key,
	department_name    			     varchar(100)
);



CREATE TABLE purpose (
	purpose_id   	         serial primary key,
	purpose_name   			     varchar(100)
);

CREATE TABLE visitor(
visitor_id   	     serial primary key,
visitor_name    	 varchar(100),
national_id          varchar(100) NOT NULL,
phone_no	         varchar(100),
visitor_org	         varchar(100),
visit_date			 date,
time_in			     time,
entity_id 			 integer references entitys,
purpose_id 			integer references purpose,
time_out			 time,
confirmation		boolean default false not null,
employee_remarks                        text
);

CREATE TABLE bookrm(
bookrm_id   	     serial primary key,
boardroom_id                 integer references boardroom,
entity_id 			 integer references entitys,
department_id		integer references department,
book_date			 date,
meet_time			     time,
end_time			 time,
reason			 text,
confirmation		boolean default false not null
);

CREATE TABLE external(
external_id   	     serial primary key,
entity_id 			 integer references entitys,
ref_date             varchar(100),
latter_date			 date,
date_received			 date,
letter_org                     varchar(100),
subject			     varchar(100),
external_details	    text,
signed_by			  varchar(100),
ceo_comment                     text,
receive_confirmation		boolean default false not null
);

CREATE TABLE internal(
internal_id   	     serial primary key,
date_received			 date,
latter_date			 date,
ref_date             varchar(100),
subject			     varchar(100),
letter_from	                varchar(100),
entity_id 			 integer references entitys,
letter_owner                     varchar(100),
receive_confirmation		boolean default false not null
);

CREATE TABLE letter_sign(
letter_sign_id   	     serial primary key,
date_in			 date,
reference               varchar(100),
details		        text,
date_out                date
);

CREATE TABLE memos(
memos_id   	     serial primary key,
date_received			 date,
subject			     varchar(100),
details                       text,
entity_id 			 integer references entitys,
approval		boolean default false not null,
financial_approval              boolean default false not null
);
