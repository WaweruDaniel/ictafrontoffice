---Views

CREATE OR REPLACE VIEW vw_purpose AS
SELECT purpose.purpose_id, purpose.purpose_name
FROM purpose;


CREATE OR REPLACE VIEW vw_department AS
SELECT department.department_id, department.department_name
FROM department;

CREATE OR REPLACE VIEW vw_employee AS
SELECT entitys.entity_id, entitys.entity_type_id, entitys.entity_name, entitys.user_name, entitys.primary_email, entitys.primary_telephone, entitys.employee_position, entitys.department_id, department.department_name
FROM department INNER JOIN entitys ON department.department_id = entitys.department_id;

CREATE OR REPLACE VIEW vw_boardroom AS
SELECT boardroom.boardroom_id, boardroom.boardroom_name
FROM boardroom;

CREATE OR REPLACE VIEW vw_bookrm AS
SELECT bookrm.bookrm_id, bookrm.boardroom_id, boardroom.boardroom_name, bookrm.entity_id, entitys.entity_name, bookrm.department_id, department.department_name, bookrm.book_date, bookrm.meet_time, bookrm.end_time, bookrm.reason, bookrm.confirmation
FROM boardroom INNER JOIN bookrm ON boardroom.boardroom_id = bookrm.boardroom_id
  INNER JOIN department ON department.department_id = bookrm.department_id
  INNER JOIN entitys ON entitys.entity_id = bookrm.entity_id;

CREATE OR REPLACE VIEW vw_visitor AS
SELECT visitor.visitor_id, visitor.visitor_name, visitor.national_id, visitor.phone_no, visitor.visitor_org, visitor.visit_date, visitor.time_in, visitor.entity_id, entitys.entity_name, visitor.purpose_id, purpose.purpose_name, visitor.time_out, visitor.confirmation, visitor.employee_remarks
FROM entitys INNER JOIN visitor ON entitys.entity_id = visitor.entity_id
  INNER JOIN purpose ON purpose.purpose_id = visitor.purpose_id;
  
  
CREATE OR REPLACE VIEW vw_ceo AS
SELECT entitys.entity_id, entitys.entity_type_id, entitys.entity_name, entitys.user_name, entitys.primary_email, entitys.primary_telephone, entitys.employee_position, entitys.department_id, department.department_name
FROM department INNER JOIN entitys ON department.department_id = entitys.department_id;

CREATE OR REPLACE VIEW vw_director AS
SELECT entitys.entity_id, entitys.entity_type_id, entitys.entity_name, entitys.user_name, entitys.primary_email, entitys.primary_telephone, entitys.employee_position, entitys.department_id, department.department_name
FROM department INNER JOIN entitys ON department.department_id = entitys.department_id;


CREATE OR REPLACE VIEW vw_finance AS
SELECT entitys.entity_id, entitys.entity_type_id, entitys.entity_name, entitys.user_name, entitys.primary_email, entitys.primary_telephone, entitys.employee_position, entitys.department_id, department.department_name
FROM department INNER JOIN entitys ON department.department_id = entitys.department_id;

  
CREATE OR REPLACE VIEW vw_reception AS
SELECT entitys.entity_id, entitys.entity_type_id, entitys.entity_name, entitys.user_name, entitys.primary_email, entitys.primary_telephone, entitys.employee_position, entitys.department_id, department.department_name
FROM department INNER JOIN entitys ON department.department_id = entitys.department_id;

CREATE OR REPLACE VIEW vw_external AS
SELECT external.external_id, external.entity_id, entitys.entity_name, external.ref_date, external.latter_date, external.date_received, external.letter_org, external.subject, external.external_details, external.signed_by, external.ceo_comment, external.receive_confirmation
FROM entitys INNER JOIN external ON entitys.entity_id = external.entity_id

CREATE OR REPLACE VIEW vw_internal AS
SELECT internal.internal_id, internal.date_received, internal.latter_date, internal.ref_date, internal.subject, internal.letter_from, internal.entity_id, entitys.entity_name, internal.letter_owner, internal.receive_confirmation
FROM entitys INNER JOIN internal ON entitys.entity_id = internal.entity_id

CREATE OR REPLACE VIEW vw_letter_sign AS
SELECT letter_sign.letter_sign_id, letter_sign.date_in, letter_sign.reference, letter_sign.details, letter_sign.date_out
FROM letter_sign

CREATE OR REPLACE VIEW vw_memos AS
SELECT memos.memos_id, memos.date_received, memos.subject, memos.details, memos.entity_id, entitys.entity_name, memos.approval, memos.financial_approval
FROM entitys INNER JOIN memos ON entitys.entity_id = memos.entity_id
